#!/usr/bin/python3

import os
import sys
from setuptools import setup, find_packages

install_data = [(os.path.join(sys.prefix, 'share/applications'), ['data/com.github.mirkobrombin.football.desktop']),
                (os.path.join(sys.prefix, 'share/metainfo'), ['data/com.github.mirkobrombin.football.appdata.xml']),
                (os.path.join(sys.prefix, 'share/pixmaps'), ['data/com.github.mirkobrombin.football.svg'])]

setup(
    name='Football',
    version='1.0.4',
    author='Mirko Brombin',
    description='Track Football scores',
    url='https://github.com/mirkobrombin/football',
    license='GNU GPL3',
    data_files=install_data,
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'com.github.mirkobrombin.football = football:main',
        ]
    }
)
