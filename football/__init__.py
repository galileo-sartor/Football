#!/usr/bin/python3
'''
   Copyright 2017 Mirko Brombin (brombinmirko@gmail.com)

   This file is part of Football.

    Football is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Football is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Football.  If not, see <http://www.gnu.org/licenses/>.
'''

import configparser
import os
import logging
import signal
import gi
import sys
import json
import requests
import time
from datetime import datetime, timedelta
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib

API_KEY = "cb3a6fe9d9284af79a13661ff6191ea6"
headers = {'X-Auth-Token':API_KEY, 'X-Response-Control': 'minified'}
stylesheet = """
    @define-color colorPrimary #249C5F;
    @define-color textColorPrimary #f2f2f2;
    @define-color textColorPrimaryShadow #197949;
""";

config = configparser.ConfigParser()
config_path = os.path.join(GLib.get_user_config_dir(), 'football.conf')
config.read(config_path)


logger = logging.getLogger(__name__)
logging.basicConfig(level='ERROR', format="%(levelname)s: %(message)s")

class Football(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Football")

        self.header_bar()

        #grid
        self.grid = Gtk.Grid()
        self.grid.set_column_homogeneous(True)
        self.grid.set_row_homogeneous(True)

        self.gen_competitions()

        try:
            logger.debug("Read config file")
            default = config.get('DEFAULT', 'default')
            self.show_latest = eval(config.get('DEFAULT', 'latest'))
            logger.debug("Read default value {}".format(default))
            self.gen_fixtures(default)
            i = -1
            for row in self.competitions_liststore:
                i = i + 1
                if row[0] == int(default):
                    self.competitions_combo.set_active(i)
        except configparser.NoOptionError as e:
            logger.debug("Config file not found. Using defaults: {}".format(e))
            self.show_latest = False
            self.gen_fixtures(self.competitions_liststore[0][0])
            self.competitions_combo.set_active(0)

        #setting up the layout
        self.scrollable_treelist = Gtk.ScrolledWindow()
        self.scrollable_treelist.set_vexpand(True)
        self.grid.attach(self.scrollable_treelist, 0, 0, 8, 10)
        self.scrollable_treelist.add(self.treeview)

        #paned
        self.paned = Gtk.Paned()
        self.paned.add1(self.grid)
        self.add(self.paned)

        #hbox
        self.hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.hbox.add(self.competitions_combo)
        self.hbar.pack_start(self.hbox)

        # last
        self.last = Gtk.Button("Last played")
        self.last = Gtk.Button.new_from_icon_name("document-open-recent", Gtk.IconSize.LARGE_TOOLBAR)
        self.last.connect("clicked", self.on_last_clicked)
        self.last.set_property("tooltip-text", "Last 7 days")
        self.hbar.pack_end(self.last)

    def header_bar(self):
        self.hbar = Gtk.HeaderBar()
        self.hbar.set_show_close_button(True)
        self.hbar.props.title = "Football"
        self.set_titlebar(self.hbar)
        Gtk.StyleContext.add_class(self.hbar.get_style_context(), "FootballHeader")

    def gen_competitions(self):
        #competition json
        try:
            competitions = requests.get("http://api.football-data.org/v2/competitions",headers=headers)
            competitions.raise_for_status()
        except requests.exceptions.HTTPError as e:
            if competitions.status_code == 429:
                logger.error("You made too many requests. Try lowering the update frequency or upgrading tier.")
                return
        competitions_obj = json.loads(competitions.text)['competitions']
        competitions_list = []
        for c in competitions_obj:
            # Change in the future to be able to add a different tier
            if c['plan'] != 'TIER_ONE':
                logger.debug("Competition {} not in free tier.".format(c['name']))
                continue
            try:
                competitions_list.append((c['id'], c['name']))
            except(KeyError):
                logger.error("Error for JSON: " + str(f))

        #competition selector
        self.competitions_liststore = Gtk.ListStore(int, str)
        for competition in competitions_list:
            self.competitions_liststore.append(list(competition))

        self.competitions_combo = Gtk.ComboBox.new_with_model_and_entry(self.competitions_liststore)
        self.competitions_combo.connect("changed", self.on_competitions_combo_changed)
        self.competitions_combo.set_entry_text_column(1)

    def gen_fixtures(self, competition_id, update=False):
         #fixtures json
        try:
            self.fixtures = requests.get(
                "http://api.football-data.org/v2/competitions/" + str(competition_id) + "/matches",headers=headers
            )
            self.fixtures.raise_for_status()
        except requests.exceptions.HTTPError as e:
            if self.fixtures.status_code == 429:
                logger.error("You made too many requests. Try lowering the update frequency or upgrading tier.")
                return
        self.fixtures_obj = json.loads(self.fixtures.text)
        logger.debug("N of fixtures: " + str(self.fixtures_obj['count']))
        self.fixtures_list = []
        for f in self.fixtures_obj['matches']:
            # Account for timezones
            offset = (time.timezone if (time.localtime().tm_isdst == 0) else time.altzone) / 60 / 60 * -1
            match_date = datetime.strptime(f['utcDate'], '%Y-%m-%dT%H:%M:%SZ') + timedelta(hours=offset)
            if str(f['score']['fullTime']['homeTeam']) == "None":
                match_results = "n/a"
            else:
                match_results = str(f['score']['fullTime']['homeTeam'])+" - " + str(f['score']['fullTime']['awayTeam'])
            match_status = f['status'].replace('_', ' ').title()
            try:
                self.fixtures_list.append((
                    f['homeTeam']['name'],
                    f['matchday'],
                    match_results,
                    f['awayTeam']['name'],
                    match_status,
                    match_date.strftime('%Y %B %d %H:%M')))
            except(KeyError):
                logger.error("Error for JSON: " + str(f))

        #fixtures selector
        if update == False:
            self.fixtures_liststore = Gtk.ListStore(str, int, str, str, str, str)
            self.last_filter = self.fixtures_liststore.filter_new()
            self.last_filter.set_visible_func(self.set_last_filter)
        else:
            self.fixtures_liststore.clear()
        for fixtures in self.fixtures_list:
            self.fixtures_liststore.append(list(fixtures))
        self.fixtures_sorted = Gtk.TreeModelSort(model=self.fixtures_liststore)
        self.fixtures_sorted.set_sort_column_id(1, Gtk.SortType.ASCENDING)
        self.treeview = Gtk.TreeView.new_with_model(self.last_filter)
        for i, column_title in enumerate(["Home team", "Day", "Score", "Away team", "Status", "Date"]):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title, renderer, text=i)
            column.set_reorderable(True)
            column.set_resizable(True)
            column.set_sort_column_id(i)
            self.treeview.append_column(column)
        GLib.timeout_add_seconds(60*3, self.on_competitions_combo_changed, self.competitions_combo)

    def on_competitions_combo_changed(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter != None:
            model = combo.get_model()
            row_id, name = model[tree_iter][:2]
            self.gen_fixtures(row_id, True)
            logger.debug("Selected: ID=%d, name=%s" % (row_id, name))
        else:
            entry = combo.get_child()
            logger.debug("Entered: %s" % entry.get_text())
        logger.debug("Set default to {}".format(row_id))
        config.set('DEFAULT', 'default', str(row_id))

    def set_last_filter(self, model, iter, data):
        if self.show_latest == True:
            today = datetime.now()
            latest = today - timedelta(days=7) # Last 7 days
            col_date = datetime.strptime(model[iter][5], '%Y %B %d %H:%M')
            return col_date <= today and col_date >= latest
        else:
            return True

    def on_last_clicked(self, widget):
        if self.show_latest == True:
            self.show_latest = False
            self.last.set_property("tooltip-text", "Last 7 days")
        else:
            self.show_latest = True
            self.last.set_property("tooltip-text", "Show all days")
        self.last_filter.refilter()
        logger.debug("Save latest setting to file.")
        logger.error("Set latest to {}".format(self.show_latest))
        config.set('DEFAULT', 'latest', str(self.show_latest))

    def destroy(self, widget, data=None):
        with open(config_path, 'w') as configfile:
            config.write(configfile)
        Gtk.main_quit()


def main():
    if '--debug' in sys.argv:
        logger.setLevel(level='DEBUG')

    style_provider = Gtk.CssProvider()
    style_provider.load_from_data(bytes(stylesheet.encode()))
    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(), style_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )

    # Glib steals the SIGINT handler and so, causes issue in the callback
    # https://bugzilla.gnome.org/show_bug.cgi?id=622084
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    win = Football()
    win.set_default_size(900, 680)
    win.connect("delete-event", win.destroy)
    win.show_all()
    Gtk.main()
