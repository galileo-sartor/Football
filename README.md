<div align="center">
  <img src="https://i.imgur.com/xqfciv5.png" width="64">
  <h1 align="center">Football</h1>
  <div align="center">An application for tracking Football scores, designed for elementary OS.</div>
</div>

</br>

<div align="center">
   <a href="https://gitlab.com/brombinmirko/Knagaroo/blob/master/LICENSE">
    <img src="https://img.shields.io/badge/License-GPL--3.0-blue.svg">
   </a>
</div>

</br>

<div align="center">
  <img src="screenshot.png">
</div>


## Requirements
- python3
- python3-requests
- gir1.2-gtk-3.0

## How to run
```bash
com.github.mirkobrombin.football
```

## Installation

### For elementary OS
[![Get it on AppCenter](https://appcenter.elementary.io/badge.svg)](https://appcenter.elementary.io/com.github.mirkobrombin.football)

### For Debian based
Grab an updated debian package [here](https://gitlab.com/brombinmirko/Football/releases)

```bash
dpkg -i com.github.mirkobrombin.football*.deb
```

### Install from source
Grab an updated release [here](https://gitlab.com/brombinmirko/Football/-/archive/master/Football-master.zip) or use **git**:

```bash
git clone https://github.com/mirkobrombin/Football.git
cd Football
sudo python3 setup.py install
```
